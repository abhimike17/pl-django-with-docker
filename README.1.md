## Steps to clone the project and install dependencies 
 1. Install Git, Python 3, pip  and any editor on your computer
 2. Clone the Project
      * $ git clone git@code.usgs.gov:NPWRC/NN0097H0701/pollinator-lib-front-end.git

 3. Install Virtual environment
    * $ pip install virtualenv.
    * $ virtualenv --version.
    * $ virtualenv my_project.

 4. Activate Virtualenv
    * $ source my_project/bin/activate.

 5. Open the project in the editor
    * cd pollinator-lib-front-end
  
 6. To install the dependencies
    * “pip install requirements.txt”
    * If that doesn’t work, open requirements.txt and copy, paste 
       each dependency
        * “pip install dependency name”
 7. Provide the database config in settings.py and follow these commands
    * DATABASES = {
        'default': {
        ENGINE': 'django.db.backends.postgresql',
        'NAME': ' ',
         'USER':’ ’,
        PASSWORD': ' ',
        'HOST': ' ',
        'PORT': ' ',
        }}
    * “Python manage.py make migrations”
    * “Python manage.py migrate
 8. To get access to the admin panel
    * “Python manage.py createsuperuser”
    * Provide credentials
 9. To run the server
    * “Python manage.py runserver”
