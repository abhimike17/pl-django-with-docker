#!/usr/bin/env bash

set -ex
docker exec -it pollinator_ui /bin/bash -c ". env/bin/activate; python3 manage.py createsuperuser"