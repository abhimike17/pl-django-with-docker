#!/usr/bin/env bash

set -ex
docker exec pollinator_ui /bin/bash -c ". env/bin/activate; python3 manage.py migrate"