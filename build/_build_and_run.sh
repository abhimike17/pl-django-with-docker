#!/bin/bash

set -ex

docker build -t usgs:pollinator src
docker run -p 8000:8000 --name pollinator_ui -v $PWD/src/pollinatorlib:/work/pollinatorlib -v $PWD/src/pollinatorapp:/work/pollinatorapp --add-host "elasticsearch:159.189.89.102" --add-host "postgresql:159.189.89.102" --rm -it usgs:pollinator
