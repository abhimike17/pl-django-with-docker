am4core.useTheme(am4themes_animated);

                            // Create chart instance
                            var chart4 = am4core.create("chartdiv4", am4charts.XYChart);
                            chart4.scrollbarY = new am4core.Scrollbar();

                            // Add data
                            chart4.data = data;

                            // Create axes
                            var categoryAxis = chart4.yAxes.push(new am4charts.CategoryAxis());
                            categoryAxis.dataFields.category = "insecttaxon";
                            categoryAxis.renderer.grid.template.location = 0;
                            categoryAxis.renderer.minGridDistance = 30;
                            categoryAxis.renderer.labels.template.horizontalCenter = "right";
                            {#categoryAxis.renderer.labels.template.verticalCenter = "right";#}
                            {#categoryAxis.renderer.labels.template.rotation = 270;#}
                            categoryAxis.tooltip.disabled = true;
                            categoryAxis.renderer.minHeight = 110;

                            var valueAxis = chart4.xAxes.push(new am4charts.ValueAxis());
                            valueAxis.renderer.minWidth = 50;


                            // Create series
                            var series = chart4.series.push(new am4charts.ColumnSeries());
                            series.sequencedInterpolation = true;
                            series.dataFields.valueX = "the_count";
                            series.dataFields.categoryY = "insecttaxon";
                            series.tooltipText = "[{categoryY}: bold]{valueX}[/]";
                            series.columns.template.strokeWidth = 0;

                            series.tooltip.pointerOrientation = "horizontal";

                            series.columns.template.column.cornerRadiusTopLeft = 0;
                            series.columns.template.column.cornerRadiusTopRight = 10;
                            series.columns.template.column.cornerRadiusBottomRight = 10;
                            series.columns.template.column.fillOpacity = 0.8;

                            // on hover, make corner radiuses bigger
                            let hoverState1 = series.columns.template.column.states.create("hover");
                            hoverState1.properties.cornerRadiusTopLeft = 0;
                            hoverState1.properties.cornerRadiusTopRight = 0;
                            hoverState1.properties.fillOpacity = 1;

                            series.columns.template.adapter.add("fill", (fill, target)=>{
                              return chart4.colors.getIndex(target.dataItem.index);
                            });

                            // Cursor
                            chart4.cursor = new am4charts.XYCursor();
                            chart4.write("chartdiv4");