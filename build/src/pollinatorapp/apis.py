from django.http import JsonResponse


from django.http import *
from django.contrib import messages
from django.shortcuts import render
from django.db.models import Q
from django.db.models import Count
from rest_framework import viewsets
from pollinatorapp.models import Pdata,Private, Publics
from .serializers import PdataSerializer, PublicSerializer, PrivateSerializer


# class PdataViewSet(viewsets.ModelViewSet):
#
#     queryset = Pdata.objects.all()
#     serializer_class = PdataSerializer


def get_search(request):
    pdata = PdataSerializer(
        Pdata.objects.all().order_by("-plkey"),
        many=True
    ).data

    return JsonResponse({"pdata": pdata})


def publics(request):
    publicdata = PublicSerializer(
        Publics.objects.all().order_by("-plkey"),
        many=True
    ).data
    return JsonResponse({"public":publicdata})


def private(request):
    privatedata = PrivateSerializer(
        Private.objects.all().order_by("-plkey"),
        many=True
    ).data
    return JsonResponse({ "private":privatedata})


def search(request):

    srch = "Asclepias syriaca"

        # insect = Pdata.objects.raw('SELECT concat_ws(' ',insectgenus, insectspecies) as insects from Public."PData"')
        # match = (Q(insect=srch) | Q(plant=srch))
        # submatch = Pdata.objects.filter(match).annotate()
        # print(submatch.count())

        # srch = request.POST['srh']

        # insect = Pdata.objects.raw('SELECT concat_ws(' ',insectgenus, insectspecies) as insects from Public."PData"')
        # match = (Q(insect=srch) | Q(plant=srch))
        # submatch = Pdata.objects.filter(match).annotate()
        # print(submatch.count())
    match = (Q(insect__icontains=srch))

    pmatch = (Q(plant__icontains=srch))

    lmatch = (Q(landuse__icontains=srch))

    allmatch = (Q(insect__icontains=srch) | Q(plant__icontains=srch))


    # if srch:
        # match = Pdata.objects.raw('SELECT COUNT(plantgenus), plantgenus, insectspecies FROM public."PData" GROUP BY plantgenus, insectspecies')

    vn = srch
    unique_list = []
    punique_list = []

    submatch = Pdata.objects.filter(match)
    plantmatch = Pdata.objects.filter(pmatch)
    landmatch = Pdata.objects.filter(lmatch)
    amatch = Pdata.objects.all()



    pfieldname = 'insect'
    pinteraction = plantmatch.values(pfieldname).order_by(pfieldname).annotate(the_count=Count(pfieldname))

    fieldname = 'plant'
    interaction = submatch.values(fieldname).order_by(fieldname).annotate(the_count=Count(fieldname))


    lfieldname = 'landuse'
    linteraction = landmatch.values(lfieldname).order_by('insectorder', fieldname).annotate(the_count=Count('insectorder', fieldname))



    landuse = []

    for p in linteraction:
        if p not in landuse:
            landuse.append(p)


    insectofield = 'insectorder'
    iinteraction = amatch.values(insectofield).order_by(insectofield).annotate(the_count=Count(insectofield))

    iord = []

    for p in iinteraction:
        if p not in iord:
            iord.append(p)

    print(iord)


    x = iord.count('insectorder')
    print(x)


    insectffield = 'insectfamily'
    ifinteraction = amatch.values(insectffield).order_by(insectffield).annotate(the_count=Count(insectffield))

    iford = []

    for p in ifinteraction:
        if p not in iford:
            iford.append(p)


    insectgfield = 'insectgenus'
    iginteraction = amatch.values(insectgfield).order_by(insectgfield).annotate(the_count=Count(insectgfield))

    igord = []

    for p in iginteraction:
        if p not in igord:
            igord.append(p)


    for p in pinteraction:
        if p not in punique_list:
            punique_list.append(p)

    my_plantorder = []

    for iorder in plantmatch.values_list('plantorder', flat=True).distinct():
        if iorder not in my_plantorder:
            my_plantorder.append(iorder)

    my_plantfamily = []

    for ifamily in plantmatch.values_list('plantfamily', flat=True).distinct():
        if ifamily not in my_plantfamily:
            my_plantfamily.append(ifamily)

    my_plongitude = []


    for i in plantmatch.values_list('longitude', flat=True).distinct():
        if i not in my_plongitude:
            my_plongitude.append(i)

    plong = []
    if my_plongitude:
        plong.append(my_plongitude[0])

    my_platitude = []
    for ifamily in plantmatch.values_list('latitude', flat=True).distinct():
        if ifamily not in my_platitude:
            my_platitude.append(ifamily)




    plat = []
    if my_platitude:
        plat.append(my_platitude[0])

    my_longitude = []

    for i in submatch.values_list('longitude', flat=True).distinct():
        if i not in my_longitude:
            my_longitude.append(i)

    long = []
    if my_longitude:
        long.append(my_longitude[0])


    my_latitude = []
    for ifamily in submatch.values_list('latitude', flat=True).distinct():
        if ifamily not in my_latitude:
            my_latitude.append(ifamily)

    lat = []
    if my_latitude:
        lat.append(my_latitude[0])



    my_plantgenus = []

    for igenus in plantmatch.values_list('plantgenus', flat=True).distinct():
        if igenus not in my_plantgenus:
            my_plantgenus.append(igenus)



    for p in interaction:
        if p not in unique_list:
            unique_list.append(p)

    my_insectorder = []

    for iorder in submatch.values_list('insectorder', flat=True).distinct():
        if iorder not in my_insectorder:
            my_insectorder.append(iorder)



    print(unique_list)
    print(punique_list)

    my_insectfamily = []

    for ifamily in submatch.values_list('insectfamily', flat=True).distinct():
        if ifamily not in my_insectfamily:
            my_insectfamily.append(ifamily)

    my_insectgenus = []

    for igenus in submatch.values_list('insectgenus', flat=True).distinct():
        if igenus not in my_insectgenus:
            my_insectgenus.append(igenus)

    if submatch:

        return HttpResponse(JsonResponse(
                      {'pr': submatch, 'if': my_insectfamily, 'ig': my_insectgenus, 'io': my_insectorder,
                       'vn': vn, 'un': unique_list,  'l': lat, 'lo': long}))


    elif pmatch:

        return HttpResponse(JsonResponse(
                      {"ppr": plantmatch, "pun": punique_list, "vn": vn, "po": my_plantorder,
                       "pg": my_plantgenus,"pf": my_plantfamily, "mpl": plat, "mol": plong}))
    # else:
    #     return HttpResponseRedirect('/search/')

    else:
        messages.error(request, 'no result found')


        # if srch:
        #     # match = Pdata.objects.raw('SELECT COUNT(plantgenus), plantgenus, insectspecies FROM public."PData" GROUP BY plantgenus, insectspecies')
        #     match = (Q(insect__icontains=srch) | Q(plant__icontains=srch))
        #
        #     submatch = Pdata.objects.filter(match)
        #
        #     mylist = []
        #     for k in submatch:
        #         k.append(str(k))
        #     print(mylist)
        #
        #     matchcount = collections.Counter(submatch)
        #     # print(matchcount)
        #
        #     if submatch:
        #
        #         return render(request, 'pages/search.html', {'sr': submatch}, {'cr': matchcount})
        #     else:
        #         messages.error(request, 'no result found')
        # else:
        #     return HttpResponseRedirect('/search/')

    # return render(request, 'scripts.js')