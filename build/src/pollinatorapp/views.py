from django.shortcuts import render
from .models import *
from django.http import *
from django.contrib import messages
from django.db.models import Count
# Create your views here.
from pollinatorapp.models import Pdata, Agriculture, Openland, Conservation, States, Other, Publics, Private, Federal, \
    searchbox
from django.db.models import Q
import json
from .serializers import PdataSerializer, PrivateSerializer, PublicSerializer, LocationsSerializer, \
    LocationsPsearchSerializer
from rest_framework import viewsets
import urllib.request
from django.urls import reverse


class PdataViewSet(viewsets.ModelViewSet):
    queryset = Pdata.objects.all()
    serializer_class = PdataSerializer


class LocationsViewSet(viewsets.ModelViewSet):
    queryset = locations.objects.all()
    serializer_class = LocationsSerializer


class LocationsPsearchViewSet(viewsets.ModelViewSet):
    queryset = locationspsearch.objects.all()
    serializer_class = LocationsPsearchSerializer


class PubdataViewSet(viewsets.ModelViewSet):
    punique_list = []
    x = Analsp.objects.all()
    queryset = x
    serializer_class = PublicSerializer


class PridataViewSet(viewsets.ModelViewSet):
    queryset = Publics.objects.all()
    serializer_class = PrivateSerializer


def interactions(searchby, fieldname, name):
    taxonlist_count = []

    landuse = searchby.filter(Q(landuse__icontains=name))

    interaction = landuse.values(fieldname).annotate(the_count=Count(fieldname))

    for p in interaction:
        if p not in taxonlist_count:
            p[name] = p.pop("the_count")
            taxonlist_count.append(p)

    return taxonlist_count


def planduse(datafilter, fieldname, name):
    searchbylanduse = datafilter.filter(Q(landuse__icontains=name))

    interaction = searchbylanduse.values(fieldname).order_by(fieldname).annotate(the_count=Count(fieldname))
    ulist = forloopfordict(interaction, name)

    return ulist


def forloopfordict(interaction, oname):
    ulist = []

    for p in interaction:
        if p not in ulist:
            p[oname] = p.pop("the_count")
            ulist.append(p)
    return ulist


def countiorderlanduse(datafilter, fieldname, oname):
    searchbylanduse = datafilter.filter(Q(insectorder__icontains=oname))

    interaction = searchbylanduse.values(fieldname).order_by(fieldname).annotate(the_count=Count(fieldname))
    ulist = forloopfordict(interaction, oname)

    return ulist


def countifamilylanduse(datafilter, fieldname, oname):
    searchbylanduse = datafilter.filter(Q(insectfamily__icontains=oname))

    interaction = searchbylanduse.values(fieldname).order_by(fieldname).annotate(the_count=Count(fieldname))
    ulist = forloopfordict(interaction, oname)

    return ulist


def forloop(searchin):
    list = []
    for k in searchin:
        if k not in list:
            list.append(k)
    return list


def frloop(searchin):
    list = []
    for k in searchin:
        list.append(k)
    return list


def namestable(plantmatch, x):
    my_plantorder = []

    for iorder in plantmatch.values_list(x, flat=True).distinct():
        if iorder not in my_plantorder:
            my_plantorder.append(iorder)

    return my_plantorder


def home(request):
    submatch = Pdata.objects.all().distinct()
    fieldname = 'planttaxon'
    interaction = submatch.values(fieldname)

    unique_list = forloop(interaction)

    antmatch = Pdata.objects.all()

    my_atitude = []
    for i in antmatch.values_list('insecttaxon', flat=True).distinct():
        if i not in my_atitude:
            my_atitude.append(i)
    for i in antmatch.values_list('planttaxon', flat=True).distinct():
        if i not in my_atitude:
            my_atitude.append(i)
    for i in antmatch.values_list('landuse', flat=True).distinct():
        if i not in my_atitude:
            my_atitude.append(i)
    xal = sorted(my_atitude)
    return render(request, 'pages/home.html', {'uniquee': unique_list, 'countplant': xal})


def countoftaxa(datafilter, name, lname):
    searchbyplant = datafilter.objects.filter(Q(planttaxon__icontains=name))
    fieldname = 'insecttaxon'
    interaction = searchbyplant.values(fieldname).order_by(fieldname).annotate(the_count=Count(fieldname))

    unique_list = forloopfordict(interaction, lname)

    searchbyinsect = datafilter.objects.filter(Q(insecttaxon__icontains=name))
    infieldname = 'planttaxon'
    ininteraction = searchbyinsect.values(infieldname).order_by(infieldname).annotate(the_count=Count(infieldname))
    inunique_list = forloopfordict(ininteraction, lname)

    return inunique_list, unique_list


def adddtodict(dict, newkey, newvalue):
    new = []
    for d in dict:
        d[newkey] = newvalue
        new.append(d)

    return new


def columnlists(value):
    landuselist = []

    landuse = Pdata.objects.values(value).distinct()

    for k in landuse:
        landuselist.append(k[value])
    return landuselist


def interactionnetwork(datafilter, taxonname, genusname):
    nativenetint = datafilter.filter((Q(plantnativestatus='Native')))
    nonnativenetint = datafilter.filter((Q(plantnativestatus='Non-Native')))
    networkinteraction = nativenetint.values(genusname, taxonname).order_by(genusname,
                                                                            taxonname).annotate(
        the_count=Count(taxonname))
    nonnatnetworkinteraction = nonnativenetint.values(genusname, taxonname).order_by(genusname,
                                                                                     taxonname).annotate(
        the_count=Count(taxonname))

    ikluc = frloop(networkinteraction)

    iklu = adddtodict(ikluc, "nodeColor", "#000000")
    kluc = frloop(nonnatnetworkinteraction)
    klu1 = adddtodict(kluc, "nodeColor", "#8B0000")
    klu = adddtodict(klu1, "linkColor", "#8B0000")

    return iklu, klu


def searchfn(datafilter, taxonname, taxonname2, srch):
    # for getting the total number of records
    interaction = datafilter.values(taxonname, 'landuse').order_by(taxonname).annotate(the_count=Count(taxonname))
    unique_list = forloop(interaction)

    # for getting the total number of records
    countofint = len(unique_list)

    # For getting the latitude and longitude locations of the plants
    my_longitude = namestable(datafilter, 'longitude')
    my_latitude = namestable(datafilter, 'latitude')

    # combining two lists into a single dictionary
    dictionary = dict(zip(my_latitude, my_longitude))

    xp = [[k, l] for k, l in dictionary.items()]

    ilatlong = forloop(xp)

    inewlist = []
    for k in ilatlong:
        jp = [float(i) for i in k]
        if jp not in inewlist:
            inewlist.append(jp)

    newuniquelist = json.dumps(unique_list)
    nativenetint = datafilter.filter((Q(plantnativestatus='Native')))
    nonnativenetint = datafilter.filter((Q(plantnativestatus='Non-Native')))

    networkinteraction = nativenetint.values(taxonname2, taxonname).order_by(taxonname2,
                                                                             taxonname).annotate(
        the_count=Count(taxonname))
    nonnatnetworkinteraction = nonnativenetint.values(taxonname2, taxonname).order_by(taxonname2,
                                                                                      taxonname).annotate(
        the_count=Count(taxonname))

    ikluc = frloop(networkinteraction)

    iklu = adddtodict(ikluc, "nodeColor", "#000000")
    kluc = frloop(nonnatnetworkinteraction)
    klu1 = adddtodict(kluc, "nodeColor", "#8B0000")
    klu = adddtodict(klu1, "linkColor", "#8B0000")

    # klu = adddtodict(klu2, "linkOpacity", 1)

    collaborators = namestable(datafilter, 'originresearchfacility')
    collabi = len(collaborators)

    statecount = namestable(datafilter, 'state')
    stcount = len(statecount)

    countinteractions = datafilter.count()

    my_atitude = namestable(datafilter, 'insecttaxon')

    return unique_list, newuniquelist, my_latitude, my_longitude, inewlist, \
           srch, my_atitude, collabi, stcount, countofint, countinteractions, iklu, klu, collaborators, statecount


def search(request):
    if request.method == "GET":
        srch = request.GET['srch']

        if srch:

            # Search using insect taxon

            # unique_list provides the output of list of planttaxon and their count depending the search
            insectgenus = 'insectgenus'
            planttaxon = 'planttaxon'
            insecttaxon = 'insecttaxon'

            submatch = Pdata.objects.filter(Q(insecttaxon__icontains=srch))
            plantmatch = Pdata.objects.filter(Q(planttaxon__icontains=srch))

            landmatch = Pdata.objects.filter(Q(landuse__icontains=srch))

            lunique_list, lnewuniquelist, lmy_latitude, lmy_longitude, linewlist, \
            srch, lmy_atitude, lcollab, lstcount, lcountofint, lcountinteractions, lklu, lklunon, lsc, lcc = searchfn(
                landmatch,
                planttaxon,
                insectgenus,
                srch)

            lorder = namestable(landmatch, 'insectorder')
            lgenus = namestable(landmatch, 'insectgenus')
            lnsectfamily = namestable(landmatch, 'insectfamily')
            lspecificepithet = namestable(landmatch, 'insectspecies')
            if "N/A" in lspecificepithet: lspecificepithet.remove("N/A")

            longlists = []

            for k in lnsectfamily:
                x = countifamilylanduse(landmatch, 'planttaxon', k)
                for l in x:
                    longlists.append(l)

            orderlists = []
            for k in lorder:
                x = countiorderlanduse(landmatch, 'planttaxon', k)
                for l in x:
                    orderlists.append(l)

            unique_list, newuniquelist, my_latitude, my_longitude, inewlist, \
            srch, my_atitude, icollab, istcount, icountofint, icountinteractions, iklu, iklunon, isc, icc = searchfn(
                submatch,
                planttaxon,
                insecttaxon,
                srch)

            for k in iklu:
                k['insecttaxon'] = srch

            for k in iklunon:
                k['insecttaxon'] = srch

            IAgriculturePlant, AgriculturePlant = countoftaxa(Agriculture, srch, "Agriculture")
            IConservationProg, ConservationProg = countoftaxa(Conservation, srch, "Conservation")
            IOpenlands, Openlands = countoftaxa(Openland, srch, "Openland")
            IFederals, Federals = countoftaxa(Federal, srch, "Federal")
            IOthers, Others = countoftaxa(Other, srch, "Other")
            IState, State = countoftaxa(States, srch, "States")

            Iprivate, private = countoftaxa(Private, srch, "Private")
            IPublic, Public = countoftaxa(Publics, srch, "Publics")
            IAll = unique_list

            Iorder = namestable(submatch, 'insectorder')
            Igenus = namestable(submatch, 'insectgenus')
            Insectfamily = namestable(submatch, 'insectfamily')

            Ispecificepithet = namestable(submatch, 'insectspecies')
            Ilands = namestable(submatch, 'landuse')
            if "N/A" in Ilands: Ilands.remove("N/A")

            if "N/A" in Ispecificepithet: Ispecificepithet.remove("N/A")

            Ilns = []

            for k in Ilands:
                x = planduse(submatch, 'planttaxon', k)
                for l in x:
                    Ilns.append(l)

            # Search using plant taxon

            plantunique_list, punique_list, my_platitude, my_plongitude, newlist, \
            srch, my_atitude, collab, stcount, countofint, countinteractions, klu, klunon, psc, pcc = searchfn(
                plantmatch,
                insecttaxon,
                planttaxon, srch)

            for k in klu:
                k['planttaxon'] = srch
                if k['insecttaxon'] == 'Apis mellifera':
                    # k['nodeColor'] = "#8B0000"
                    k['linkColor'] = "#8B0000"

            klu = sorted(klu, key=lambda k: k['insecttaxon'])

            for k in klunon:
                k['planttaxon'] = srch
                k['nodeColor'] = "#000000"
                k['linkColor'] = "#000000"

            for k in klunon:
                if k['insecttaxon'] == 'Apis mellifera':
                    # k['nodeColor'] = "#8B0000"
                    k['linkColor'] = "#8B0000"
            klunon = sorted(klunon, key=lambda k: k['insecttaxon'])

            All = punique_list

            my_atitude = namestable(plantmatch, 'insecttaxon')

            listofusesforplants = []
            listofusesforinsects = []
            order = namestable(plantmatch, 'plantorder')
            genus = namestable(plantmatch, 'plantgenus')
            plantfamily = namestable(plantmatch, 'plantfamily')
            specificepithet = namestable(plantmatch, 'plantspecies')
            plands = namestable(plantmatch, 'landuse')
            if "N/A" in plands: plands.remove("N/A")

            if "N/A" in specificepithet: specificepithet.remove("N/A")

            plns = []

            for k in plands:
                x = planduse(plantmatch, 'insecttaxon', k)
                for l in x:
                    plns.append(l)

            if submatch:

                return render(request, 'pages/search.html',
                              {'pr': submatch, 'nun': newuniquelist,
                               'un': unique_list, 'l': my_latitude, 'lo': my_longitude, 'location': inewlist,
                               'IAG': IAgriculturePlant,
                               'ICP': IConservationProg, 'IOP': IOpenlands, 'IFed': IFederals, 'IO': IOthers,
                               'Istate': IState, 'Iprivate': Iprivate, 'Ipublic': IPublic,
                               'IAll': IAll, 'plantinteraction': iklu, 'nonplantinteraction': iklunon, 'Iorder': Iorder,
                               'Igenus': Igenus, 'Insectfamily': Insectfamily, 'Ispecific': Ispecificepithet,
                               'srch': srch,
                               'countplant': my_atitude, 'collab': icollab, 'statecount': istcount,
                               'countofint': icountofint, 'countinteractions': icountinteractions, 'ilns': Ilns,
                               'ilands': Ilands, 'statename': isc, 'collabs': icc})


            elif plantmatch:

                return render(request, 'pages/search.html',
                              {'ppr': plantmatch, 'pun': punique_list, 'pn': plantunique_list,
                               'AG': AgriculturePlant,
                               'CP': ConservationProg, 'OP': Openlands, 'Fed': Federals, 'O': Others, 'state': State,
                               'private': private,
                               'public': Public,
                               'All': All, 'mpl': my_platitude,
                               'mol': my_plongitude,
                               'dict': newlist, 'insectinteraction': klu, 'noninsectinteraction': klunon,
                               'order': order,
                               'genus': genus, 'plantfamily': plantfamily, 'specific': specificepithet, 'srch': srch,
                               'statecount': stcount, 'countinteractions': countinteractions, 'countofint': countofint,
                               'collab': collab, 'plns': plns, 'plands': plands, 'statename': psc, 'collabs': pcc})

            elif landmatch:
                return render(request, 'pages/search.html',
                              {'lr': landmatch, 'lunique_list': lunique_list, 'lnewuniquelist': lnewuniquelist,
                               'lmy_latitude': lmy_latitude, 'lmy_longitude': lmy_longitude, 'linewlist': linewlist,
                               'srch': srch, 'lmy_atitude': lmy_atitude,
                               'countofint': lcountofint, 'statecount': lstcount,
                               'countinteractions': lcountinteractions, 'lklu': lklu,
                               'lklunon': lklunon, 'collab': lcollab, 'lorder': lorder, 'lgenus': lgenus,
                               'lnsectfamily': lnsectfamily, 'lspecificepithet': lspecificepithet,
                               'stackedorder3': longlists, 'stackedorder3a': orderlists, 'statename': lcc,
                               'collabs': lsc})



            else:
                messages.error(request, 'no result found')
        else:
            return HttpResponseRedirect(reverse('/search/', kwargs={'srch': srch}))

    return render(request, 'pages/search.html')


# def field_list(request):
#     if request.method == "POST":
#         srch = request.POST['genus']
#
#         if srch:
#
#             # Search using insect taxon
#
#             # unique_list provides the output of list of planttaxon and their count depending the search
#             insectgenus = 'insectgenus'
#             planttaxon = 'planttaxon'
#             plantgenus = 'plantgenus'
#             insecttaxon = 'insecttaxon'
#
#             submatch = Pdata.objects.filter(Q(insecttaxon__icontains=srch))
#             plantmatch = Pdata.objects.filter(Q(planttaxon__icontains=srch))
#
#             unique_list, newuniquelist, my_latitude, my_longitude, inewlist, \
#             srch, my_atitude, collabi, stcount, countofinti, countinteractionsi, iklu, iklunon = searchfn(submatch,
#                                                                                                           planttaxon,
#                                                                                                           insectgenus,
#                                                                                                           srch)
#             IAgriculturePlant, AgriculturePlant = countoftaxa(Agriculture, srch, "Agriculture")
#             IConservationProg, ConservationProg = countoftaxa(Conservation, srch, "Conservation")
#             IOpenlands, Openlands = countoftaxa(Openland, srch, "Openland")
#             IFederals, Federals = countoftaxa(Federal, srch, "Federal")
#             IOthers, Others = countoftaxa(Other, srch, "Other")
#             IState, State = countoftaxa(States, srch, "States")
#
#             Iprivate, private = countoftaxa(Private, srch, "Private")
#             IPublic, Public = countoftaxa(Publics, srch, "Publics")
#             IAll = unique_list
#
#             Iorder = namestable(submatch, 'insectorder')
#             Igenus = namestable(submatch, 'insectgenus')
#             Insectfamily = namestable(submatch, 'insectfamily')
#             Ispecificepithet = namestable(submatch, 'insectspecies')
#
#             # Search using plant taxon
#
#             plantunique_list, punique_list, my_platitude, my_plongitude, newlist, \
#             srch, my_atitude, collab, pstcount, countofint, countinteractions, klu, klunon = searchfn(plantmatch,
#                                                                                                       insecttaxon,
#                                                                                                       plantgenus, srch)
#
#             All = punique_list
#
#             my_atitude = []
#             for i in plantmatch.values_list('insecttaxon', flat=True).distinct():
#                 if i not in my_atitude:
#                     my_atitude.append(i)
#
#             RA, RAP = planduse(Agriculture, srch, "Rangeland")
#             farmlandlist, farmlandlistplant = planduse(Agriculture, srch, "Farmland")
#             Agrilandlist, Agrilandlistplant = planduse(Agriculture, srch, "Agriculturalresearch")
#             AGR, AGRP = planduse(Agriculture, srch, "Agriculturalcropproduction")
#             UC, UCP = planduse(Conservation, srch, "Usdaconservationreserveprogram")
#             UCRP, UCRPP = planduse(Conservation, srch, "Usdaconservationreservepractice42")
#             UWRP, UWRPP = planduse(Conservation, srch, "Usdawetlandreserveprogram")
#             EQIP, EQIPP = planduse(Conservation, srch,
#                                    "Environmentalqualityincentivesprogram")
#             NC, NCP = planduse(Conservation, srch, "Natureconservancy")
#             UGRP, UGRPP = planduse(Conservation, srch, "Usdagrasslandreserveprogram")
#             UCSP, UCSPP = planduse(Conservation, srch, "Usdaconservationstewardshipprogram")
#             FW, FWP = planduse(Openland, srch, "Forest")
#             OG, OGP = planduse(Openland, srch, "Othergrassland")
#             UNP, UNPP = planduse(Federal, srch, "Usnpsnationalpark")
#             NHL, NHLP = planduse(Federal, srch, "Nationalhistoriclandmark")
#             UNWR, UNWRP = planduse(Federal, srch, "Usfwsnationalwildliferefuge")
#             UWPA, UWPAP = planduse(Federal, srch, "Usfwswaterfowlproductionarea")
#             UNFR, UNFRP = planduse(Federal, srch, "Usfsnationalforestrangeland")
#             UNF, UNFP = planduse(Federal, srch, "Usfsnationalforest")
#             OPL, OPLP = planduse(Other, srch, "Otherpubliclands")
#             OHNR, OHNRP = planduse(States, srch, "Ohdeptofnaturalresources")
#             RL, RLP = planduse(Other, srch, "Roadside")
#             MOC, MOCP = planduse(States, srch, "Modeptofconservation")
#             MNWMA, MNWMAP = planduse(States, srch, "Mnwildlifemanagementarea")
#             NDWMA, NDWMAP = planduse(States, srch, "Ndwildlifemanagementarea")
#
#             order = namestable(plantmatch, 'plantorder')
#             genus = namestable(plantmatch, 'plantgenus')
#             plantfamily = namestable(plantmatch, 'plantfamily')
#             specificepithet = namestable(plantmatch, 'plantspecies')
#
#             if submatch:
#
#                 return render(request, 'pages/example.html',
#                               {'pr': submatch, 'nun': newuniquelist, 'RA': RA, 'UNWR': UNWR, 'UCRP': UCRP,
#                                'un': unique_list, 'l': my_latitude, 'lo': my_longitude, 'location': inewlist,
#                                'IAG': IAgriculturePlant,
#                                'ICP': IConservationProg, 'IOP': IOpenlands, 'IFed': IFederals, 'IO': IOthers,
#                                'Istate': IState,
#                                'Iprivate': Iprivate,
#                                'Ipublic': IPublic, 'AGR': AGR, 'UWRP': UWRP, 'UC': UC,
#                                'IAll': IAll,
#                                'Ifarmlandlist': farmlandlist,
#                                'Agrilandlist': Agrilandlist, 'EQIP': EQIP, 'UCSP': UCSP,
#                                'NC': NC, 'FW': FW, 'OG': OG, 'UNP': UNP, 'NHL': NHL, 'UGRP': UGRP,
#                                'UWPA': UWPA, 'UNF': UNF, 'OPL': OPL,
#                                'OHNR': OHNR, 'RL': RL, 'MOC': MOC, 'MNWMA': MNWMA, 'NDWMA': NDWMA,
#                                'plantinteraction': iklu, 'Iorder': Iorder, 'Igenus': Igenus,
#                                'Insectfamily': Insectfamily, 'UNFR': UNFR, 'Ispecific': Ispecificepithet, 'srch': srch,
#                                'countplant': my_atitude, 'collabi': collabi, 'statecounti': stcount,
#                                'countofinti': countofinti,
#                                'countinteractionsi': countinteractionsi, 'nonplantinteraction': iklunon, })
#
#
#             elif plantmatch:
#
#                 return render(request, 'pages/example.html',
#                               {'ppr': plantmatch, 'pun': punique_list, 'pn': plantunique_list, 'UNWRP': UNWRP,
#                                'AG': AgriculturePlant,
#                                'CP': ConservationProg, 'OP': Openlands, 'Fed': Federals, 'O': Others, 'state': State,
#                                'private': private,
#                                'public': Public, 'RAP': RAP, 'UCRPP': UCRPP, 'AGRP': AGRP, 'UWRPP': UWRPP, 'UCP': UCP,
#                                'All': All, 'mpl': my_platitude,
#                                'mol': my_plongitude,
#                                'dict': newlist, 'farmlandlistplant': farmlandlistplant,
#                                'Agrilandlistplant': Agrilandlistplant, 'EQIPP': EQIPP, 'UCSPP': UCSPP,
#                                'NCP': NCP, 'FWP': FWP, 'OGP': OGP, 'UNPP': UNPP, 'NHLP': NHLP, 'UGRPP': UGRPP,
#                                'UWPAP': UWPAP, 'UNFP': UNFP, 'OPLP': OPLP,
#                                'OHNRP': OHNRP, 'RLP': RLP, 'MOCP': MOCP, 'MNWMAP': MNWMAP, 'NDWMAP': NDWMAP,
#                                'farmlist': farmlandlistplant, 'insectinteraction': klu, 'noninsectinteraction': klunon,
#                                'UNFRP': UNFRP, 'order': order,
#                                'genus': genus, 'plantfamily': plantfamily, 'specific': specificepithet, 'srch': srch,
#                                'statecount': pstcount, 'countinteractions': countinteractions, 'countofint': countofint,
#                                'collab': collab})
#
#
#             else:
#                 messages.error(request, 'no result found')
#         else:
#             return HttpResponseRedirect('/genus/')
#
#         return render(request, 'pages/example.html')


def contribute(request):
    return render(request, 'pages/contribute.html', {})
