from django_elasticsearch_dsl import DocType, Index, fields
from .models import Pdata

# Name of the Elasticsearch index
search_index = Index('library')
# See Elasticsearch Indices API reference for available settings
search_index.settings(
    number_of_shards=1,
    number_of_replicas=0
)
