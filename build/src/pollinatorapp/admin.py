from django.contrib import admin
from pollinatorapp.models import Agriculture
from pollinatorapp.models import Conservation
from pollinatorapp.models import Openland
from pollinatorapp.models import Federal
from pollinatorapp.models import States
from pollinatorapp.models import Private
from pollinatorapp.models import Publics
from pollinatorapp.models import Pdata

# Register your models here.

admin.site.register(Pdata)
admin.site.register(Agriculture)
admin.site.register(Conservation)
admin.site.register(Openland)
admin.site.register(Publics)
admin.site.register(Federal)
admin.site.register(States)
admin.site.register(Private)



