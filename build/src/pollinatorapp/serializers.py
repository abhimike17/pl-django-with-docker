from rest_framework import serializers

from pollinatorapp.models import Pdata, Publics, Private, Analsp,locations, locationspsearch


class PdataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pdata
        fields = ("insecttaxon", "planttaxon", "latitude", "longitude", "landuse", "insectgenus", "plantgenus")


class LocationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = locations
        fields = ("insecttaxon", "planttaxon", "latitude", "longitude", "count", "landuse")


class LocationsPsearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = locationspsearch
        fields = ("insecttaxon", "planttaxon", "latitude", "longitude", "count", "landuse")


class PublicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Analsp
        fields = ("planttaxon", "insectgenus", "the_count")


class PrivateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Private
        fields = ("insecttaxon", "planttaxon", "insectgenus", "landuse")

# class PdataSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Pdata
#         fields = ("insecttaxon", "planttaxon", "latitude", "longitude", "landuse")
#
# class PdataSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Pdata
#         fields = ("insecttaxon", "planttaxon", "latitude", "longitude", "landuse")
#
# class PdataSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Pdata
#         fields = ("insecttaxon", "planttaxon", "latitude", "longitude", "landuse")
#
# class PdataSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Pdata
#         fields = (
#         "insecttaxon", "planttaxon", "latitude", "longitude", "landuse")
#
#         # class PdataSerializer(serializers.HyperlinkedModelSerializer):
#         #     class Meta:
#         #         model = Pdata
#         #         fields = ("plkey", "url", "pollinatorlibraryid", "physicallocationofinsectspecimen", "insect", "plant")
