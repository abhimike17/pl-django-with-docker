from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('pdatas', views.PdataViewSet)
router.register('public', views.PubdataViewSet)
router.register('private', views.PridataViewSet)
router.register('locations', views.LocationsViewSet)
router.register('locationspsearch', views.LocationsPsearchViewSet)


urlpatterns = [
    path('', include(router.urls))

]
