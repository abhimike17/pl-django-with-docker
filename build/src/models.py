# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class InsectSpecies(models.Model):
    insect = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return self.insect


class Pdata(models.Model):
    id = models.IntegerField(blank=True, null=True)
    pollinatorlibraryid = models.CharField(max_length=45, blank=True, null=True)
    physicallocationofinsectspecimen = models.CharField(max_length=100, blank=True, null=True)
    determinedby = models.CharField(max_length=100, blank=True, null=True)
    insectorder = models.CharField(max_length=45, blank=True, null=True)
    insectfamily = models.CharField(max_length=45, blank=True, null=True)
    insectsubfamily = models.CharField(max_length=45, blank=True, null=True)
    insecttribe = models.CharField(max_length=45, blank=True, null=True)
    insectgenus = models.CharField(max_length=45, blank=True, null=True)
    insectsubgenus = models.CharField(max_length=45, blank=True, null=True)
    insectspecies = models.CharField(max_length=45, blank=True, null=True)
    sex = models.CharField(max_length=15, blank=True, null=True)
    date = models.CharField(max_length=15, blank=True, null=True)
    time = models.CharField(max_length=20, blank=True, null=True)
    timezone = models.CharField(max_length=5, blank=True, null=True)
    collectedby = models.CharField(max_length=100, blank=True, null=True)
    collectionmethod = models.CharField(max_length=45, blank=True, null=True)
    observedbehaviorofinsectvisitor = models.CharField(max_length=45, blank=True, null=True)
    plantorder = models.CharField(max_length=45, blank=True, null=True)
    plantfamily = models.CharField(max_length=45, blank=True, null=True)
    plantgenus = models.CharField(max_length=45, blank=True, null=True)
    plantspecies = models.CharField(max_length=45, blank=True, null=True)
    plantvariety = models.CharField(max_length=45, blank=True, null=True)
    plantnativestatus = models.CharField(max_length=45, blank=True, null=True)
    insectfieldid = models.CharField(max_length=45, blank=True, null=True)
    landuse = models.CharField(max_length=100, blank=True, null=True)
    sitedescription = models.CharField(max_length=255, blank=True, null=True)
    conservationpracticenumber = models.CharField(max_length=15, blank=True, null=True)
    sitename = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    county = models.CharField(max_length=45, blank=True, null=True)
    elevation = models.CharField(max_length=15, blank=True, null=True)
    longitude = models.CharField(max_length=15, blank=True, null=True)
    latitude = models.CharField(max_length=15, blank=True, null=True)
    averagewindspeed = models.CharField(max_length=15, blank=True, null=True)
    temperature = models.CharField(max_length=15, blank=True, null=True)
    relativehumidity = models.CharField(max_length=15, blank=True, null=True)
    originresearchfacility = models.CharField(max_length=100, blank=True, null=True)
    contact = models.CharField(max_length=45, blank=True, null=True)
    contactemail = models.CharField(max_length=45, blank=True, null=True)
    contactaddress = models.CharField(max_length=255, blank=True, null=True)
    contactphonenumber = models.CharField(max_length=15, blank=True, null=True)
    comments = models.CharField(max_length=255, blank=True, null=True)
    insect = models.ForeignKey(InsectSpecies, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'PData'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
