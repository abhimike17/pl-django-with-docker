"""pollinatorlib URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from pollinatorapp import views, apis
# from djgeojson.views import GeoJSONLayerView
from rest_framework import routers

# router = routers.DefaultRouter()
# router.register('pdata/', apis.PdataViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    # path('genus/', views.field_list),
    path('search/<slug:srch>/', views.search,name = 'srch'),
    path('search/', views.search),

    # path('search/', views.search),

    path('', include('pollinatorapp.urls')),
    path('contribute/', views.contribute),
    path('api/pdata', apis.get_search),
    path('api/public', apis.publics),
    path('api/private', apis.private),
    # path('autocomplete/',views.autocomplete, name='autocomplete'),
    path('api/search/', apis.search)]
